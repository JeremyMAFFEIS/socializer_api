// User.js

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Define collection and schema for Items
var User = new Schema({
  email: {
    type: String
  },
  name: {
    type: String
  },
  nom: {
    type: String
  },
  prenom: {
    type: String
  }
},{
    collection: 'users'
});

module.exports = mongoose.model('User', User);